package siruri;
import java.util.*;

public class Sir {
    private int[] sir;
    int len;
    public Sir(){
        sir =  new int[6];
    }
    public Sir(int len){ // construnctorul clasei. Primeste lungimea sirului
        sir = new int[len];
        Random random = new Random();
        for(int i = 0; i < len; i++){
            do {
                sir[i] = 1 + random.nextInt(49); //generam elementele sirului in mod aleator intre [1, 49]
            }while (esteNrInSir(sir[i], i) == true);
        }
    }
    public Sir(float lungime) { // i-am facut alta amprenta sa putem citi de la tastatura valorile
        sir = new int[(int)lungime];
        System.out.println("Introduceri cele " + lungime + " nunere pentru loto");
        Scanner in = new Scanner(System.in);
        for(int i = 0; i < lungime; i++){
            sir[i] = in.nextInt();
        }
    }

    public Sir(int len, int max){  // alt contructor ce primeste lungimea sirului si valoarea maxima a elelmentelor din sir
        sir = new int[len];
        Random random = new Random();
        for(int i = 0; i < len; i++){
            sir[i] = random.nextInt(max) ;
        }
    }

    public void afisareSir(){ // metoda ce afiseaza sirul pe ecran
        for (int i = 0; i < this.sir.length; i++){
            System.out.print(this.sir[i] + " "); // "this" e optional in cazul asta
        }
        System.out.println();
    }
    public void afisareSirFaraFor(){
        System.out.println(Arrays.toString(sir));
    }
    public int nrAparitii(int x){ // metoda ce verifica de cate ori apare un numar in sirul nostru
        int nrAparitii = 0; // contor pentru numararea aparitiilor elementului x in sir
        for(int i = 0; i < sir.length; i++){
            if (sir[i] == x){
                nrAparitii++;
            }
        }
        return nrAparitii; // returneaza numarul de aparitii
    }
    public boolean esteNrInSir(int nr){
        for(int i = 0; i < this.sir.length; i++){
            if (nr == this.sir[i]){
                return true;
            }
        }
        return false;
    }
    public boolean esteNrInSir(int nr, int pozitie){
        for(int i = 0 ; i < pozitie; i++){
            if (nr == this.sir[i]){
                return true;
            }
        }
        return false;
    }
    private boolean esteNrPrim(int nr){
        if (nr <= 1){
            return false;
        }
        if (nr == 2){
            return true;
        }
        if ((nr % 2) == 0){
            return false;
        }
        int radical = (int)Math.sqrt(nr);
        for (int i = 3; i <= radical; i+=2){
            if ((nr % i) == 0){
                return false;
            }
        }
        return true;
    }
    public int nrNumerePrimeInSir(){
        int nrNumerePrime = 0;
        for(int x: this.sir){
            if (esteNrPrim(x) == true){
                nrNumerePrime++;
            }
        }
        return nrNumerePrime;
    }

    public int numereInComun(Sir numereExtrase){
        int numereGhicite = 0;
        for(int i = 0; i < 6; i++){
            for(int j = 0; j < 6; j++){
                if (this.sir[i] == numereExtrase.sir[j]){
                    numereGhicite++;
                }
            }
        }
        return numereGhicite;
    }
    boolean cautareBinara(int x)
    {
        int ini = 0, sf = sir.length - 1;
        while (ini <= sf) {
            int poz = (ini + sf) / 2;

            if (sir[poz] == x)
                return true;

            if (sir[poz] < x) {
                ini = poz + 1;
            }
            else {
                sf = poz - 1;
            }
        }

        return false;
    }
}

