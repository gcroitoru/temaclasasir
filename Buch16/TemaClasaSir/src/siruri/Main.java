package siruri;

import java.util.Scanner;

class Main {
    public static void main(String[] args) {
        Sir sir1 = new Sir(15, 10); // declar un obiect de tip Sir de lungime 15 ce va avea elemente de la 0 la 9
        Sir numereDeJucat = new Sir((float)6);
        System.out.print("Numerele pe care le joc sunt: ");
        numereDeJucat.afisareSir();
        System.out.println();
        numereDeJucat.afisareSirFaraFor();


        Sir[] varianteLoto = new Sir[20];
        Sir[] varianteLoto2NumereComune = new Sir[20];
        int[] varianteLotoNumereComune = new int[20];

        for(int i = 1; i <= 20; i++) { // simulan 20 extrageri loto
            varianteLoto[i - 1] = new Sir(6); // generam numele extrase
            System.out.print("Numere loto varianta " + i + ": ");
            varianteLoto[i - 1].afisareSir();
            varianteLotoNumereComune[i-1] = numereDeJucat.numereInComun(varianteLoto[i - 1]);
            System.out.printf(" Eu am prins %d numere!", numereDeJucat.numereInComun(varianteLoto[i - 1])); //verificam numerele
            System.out.print("\n---------------------------\n");
        }
        int contorVariante2NumereComune = 0;
        for(int i = 0; i < 20; i++){
            if (varianteLotoNumereComune[i] == 2){
                varianteLoto2NumereComune[contorVariante2NumereComune++] = varianteLoto[i];
                varianteLoto[i].afisareSir();
                System.out.println();
            }
        }
 /*
        sir1.afisareSir(); // apelez metoda ce afiseaza sirul pe ectan
        Scanner input = new Scanner(System.in);
        System.out.print("\nIntroduceti numarul de cautat in sir: ");
        int nr = input.nextInt();

        System.out.println("\nNumarul " + nr + " apare in sir1 de " + sir1.nrAparitii(nr) + " ori\n");
        System.out.println("In sir1 se afla " + sir1.nrNumerePrimeInSir() + " numere prime!");
        Sir sir2 = new Sir(10);
        sir2.afisareSir();
        System.out.println("In sir2 se afla " + sir2.nrNumerePrimeInSir() + " numere prime!");
*/
    }
}