package loop.while_loop;

import java.util.Scanner;

public class Example2 {

    public static void main(String[] args) {
        Scanner tastatura = new Scanner(System.in);
        String cuvant;
        //int contor = 0;

        while (true) {
            System.out.print("Introduceti cuvant: ");
            cuvant = tastatura.nextLine();
            if (cuvant.equalsIgnoreCase("exit")){
                System.out.println("Ai introdus exit!");
               break;
            }
            System.out.println(cuvant.toUpperCase());
            //contor++;
       }

    }

}
